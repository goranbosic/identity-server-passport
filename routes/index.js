var express = require('express');
var passport = require('passport');
var GrandIdStrategy = require('passport-grandid');
var router = express.Router();
var config = require('config');
const User = require('./../models/user.js');


passport.use(new GrandIdStrategy({
        sessionid: '123456',
        apiKey: config.get('GrandId.api.apiKey'),
        authenticateServiceKey: config.get('GrandId.api.authenticateServiceKey'),
        url: "https://client-test.grandid.com/json1.1/GetSession"
    },
    function(req, done) {}
));
/* GET home page. */
router.get('/login',
    passport.authenticate('grandid', {
        sessionid: '123456',
        apiKey: config.get('GrandId.api.apiKey'),
        authenticateServiceKey: config.get('GrandId.api.authenticateServiceKey'),
        url: "https://client-test.grandid.com/json1.1/GetSession"}),
    function(req, res) {
        User.findOrCreate({ personal_number: res.personal_number }, function (err, user) {
            return user;
        });
            res.redirect('/');
    });


module.exports = router;
